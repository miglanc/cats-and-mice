package catsandmouse.player;

public class Cat extends Player {

    private static char nextId = 65;

    {
        setId(nextId);
        nextId++;
    }

    public Cat() {
        super.setPlayerMoveChoices(new int[]{2, 4, 5, 6, 8});
    }

    @Override
    public String toString() {
        return String.format("Cat %s", getId());
    }
}
