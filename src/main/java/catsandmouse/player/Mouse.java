package catsandmouse.player;

public class Mouse extends Player {

    private static char nextId = 65;

    {
        setId(nextId);
        nextId++;
    }

    public Mouse() {
        super.setPlayerMoveChoices(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9});
    }

    @Override
    public String toString() {
        return String.format("Mouse %s", getId());
    }
}
