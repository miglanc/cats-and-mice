package catsandmouse.player;

import catsandmouse.game.Board;
import catsandmouse.Position;

import java.util.Random;

public abstract class Player {

    private char id;
    private Position position;
    private boolean isAlive;
    private int[] playerMoveChoices;

    public Player() {
        this.position = new Position();
        this.isAlive = true;
    }

    public Position move(int numKey, int x, int y) {
        switch (numKey) {
            case 1 :    x++; y--;
                break;
            case 2 :    y++;
                break;
            case 3 :    x++; y++;
                break;
            case 4 :    y--;
                break;
            case 5 :
                break;
            case 6 :    y++;
                break;
            case 7 :    x--; y--;
                break;
            case 8 :    x--;
                break;
            case 9 :    x--; y++;
                break;
        }
        getPosition().setX(x);
        getPosition().setY(y);
        return getPosition();
    }

    public  void playerRandomMove() {
        Random random = new Random();
        boolean playerMoved = false;
        int startX = getPosition().getX();
        int startY = getPosition().getY();

        int newX=0, newY=0;
        while (!playerMoved) {
            int index = random.nextInt(playerMoveChoices.length);
            int numKey = playerMoveChoices[index];
            int size = Board.BOARD_SIZE;
            Position newPosition = move(numKey, startX, startY);
            newX = newPosition.getX();
            newY = newPosition.getY();
            if (newX >= 0 && newX < size && newY >= 0 && newY < size) {
                getPosition().setX(newX);
                getPosition().setY(newY);
                playerMoved = true;
            }
        }
        System.out.printf("%s: (%s,%s) -> (%s,%s)\n", toString(), startX, startY, newX, newY);
    }

    // getters and setters ================================================

    public Position getPosition() {
        return position;
    }

    public char getId() {
        return id;
    }

    public void setId(char id) {
        this.id = id;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public void setPlayerMoveChoices(int[] playerMoveChoices) {
        this.playerMoveChoices = playerMoveChoices;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }
}
