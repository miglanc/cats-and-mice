package catsandmouse.game;

import catsandmouse.Position;

import java.util.Scanner;

public class Board {

    public static int BOARD_SIZE;
    static Position cheesePosition ;
    private static Scanner input = new Scanner(System.in);

    private BoardField[][] boardFields;

    Board() {
        System.out.println("Enter board size: ");
        BOARD_SIZE = input.nextInt(); input.nextLine();
        if (BOARD_SIZE % 2 == 0) {
            throw new IllegalArgumentException("Board size should be odd number!");
        }
        cheesePosition = new Position(BOARD_SIZE / 2, BOARD_SIZE / 2);

        this.boardFields = new BoardField[BOARD_SIZE][BOARD_SIZE];
        for (int i = 0; i < boardFields.length; i++) {
            for (int j = 0; j < boardFields.length; j++) {
                BoardField boardField = new BoardField();
                Position position = new Position(i,j);
                boardFields[i][j] = boardField;
                boardField.setPosition(position);
            }
        }
        boardFields[BOARD_SIZE/2][BOARD_SIZE/2].setSgn("S");
    }

    void setNewSgnsOnBoard() {
        BoardField[][] boardFields = this.boardFields;
        for (BoardField[] boardField : boardFields) {
            for (int j = 0; j < boardFields.length; j++) {

                BoardField currentBoardField = boardField[j];
                Position currentPosition = boardField[j].getPosition();
                int catsNumber = boardField[j].getCatsNumber();
                int miceNumber = boardField[j].getMiceNumber();

                if (currentPosition.equals(cheesePosition)) {
                    if (catsNumber > 0 && miceNumber > 0) {
                        currentBoardField.setSgn("SCM");
                    } else if (catsNumber > 0) {
                        currentBoardField.setSgn("SC");
                    } else if (miceNumber > 0) {
                        currentBoardField.setSgn("SM");
                    } else {
                        currentBoardField.setSgn("S");
                    }
                } else {
                    if (catsNumber > 0) {
                        currentBoardField.setSgn("C");
                    } else if (miceNumber > 0) {
                        currentBoardField.setSgn("M");
                    } else {
                        currentBoardField.setSgn("_");
                    }
                }
            }
        }
    }

    Position[] generateEdgePositions() {
        Position[] edgePositions = new Position[4 * (BOARD_SIZE - 1)];
        for (int i = 0; i < edgePositions.length; i++) {
            Position position = new Position();
            if (i < BOARD_SIZE - 1) {
                position.setX(0);
                position.setY(i);
            } else if (i < 2 * (BOARD_SIZE - 1)) {
                position.setX(i - BOARD_SIZE + 1);
                position.setY(BOARD_SIZE - 1);
            } else if (i < 3 * (BOARD_SIZE - 1)) {
                position.setX(BOARD_SIZE - 1);
                position.setY(3 * (BOARD_SIZE - 1) - i);
            } else {
                position.setX(4 * (BOARD_SIZE - 1) - i);
                position.setY(0);
            }
            edgePositions[i] = position;
        }
        return edgePositions;
    }

    BoardField findBoardField(Position position) {
        for (BoardField[] boardField : boardFields) {
            for (BoardField f : boardField) {
                if (f.getPosition().equals(position)) {
                    return f;
                }
            }
        }
        return null;
    }

    void displayBoard() {
        for (BoardField[] boardField : boardFields) {
            for (int j = 0; j < boardFields.length; j++) {
                System.out.print(boardField[j].getSgn() + " ");
            }
            System.out.println();
        }
    }
}