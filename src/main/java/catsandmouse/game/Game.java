package catsandmouse.game;

import catsandmouse.Position;
import catsandmouse.player.Cat;
import catsandmouse.player.Mouse;
import catsandmouse.player.Player;

import java.util.Random;
import java.util.Scanner;

public class Game {

    private static Scanner input = new Scanner(System.in);
    private static Random random = new Random();

    private Board board;
    private Cat[] cats;
    private Mouse[] mice;

    public Game() {

        this.board = new Board();

        System.out.println("Enter Mice number: ");
        final int MAX_MICE = input.nextInt();
        input.nextLine();

        System.out.println("Enter Cats number: ");
        final int MAX_CATS = input.nextInt();
        input.nextLine();

        this.cats = new Cat[MAX_CATS];
        this.mice = new Mouse[MAX_MICE];
    }

    public void run() {
        locateMiceEvenly();
        locateCats();
        board.displayBoard();

        int count = 1;
        boolean isGameFinished = false;

        while (!isGameFinished) {
            System.out.println("Round number " + count + ": ");
            System.out.println("Number of alive mice: " + countAliveMice());
            allPlayersTurn();
            killMice();
            board.displayBoard();

            if (countAliveMice() == 0) {
                System.out.println("Cats won!");
                break;
            } else if (board.findBoardField(Board.cheesePosition).getSgn().equals("SM")) {
                System.out.printf("Mouse %s reached the cheese\n", findMouseOnCheesePosition().getId());
                System.out.println("Mice won!");
                break;
            } else if (board.findBoardField(Board.cheesePosition).getSgn().equals("SCM")) {
                System.out.println("A tie!");
                break;
            }

            System.out.print("Next round? (y/n): ");
            String answer = input.nextLine();
            if (!answer.equalsIgnoreCase("y")) {
                System.out.println("Game interrupted.");
                isGameFinished = true;
            }
            count++;
        }
    }

    private void allPlayersTurn() {
        Player[] players = combinePlayers();
        for (Player player : players) {
            if (player.isAlive()) {

                Position playerStartPosition = player.getPosition();
                BoardField boardOldField = board.findBoardField(playerStartPosition);

                player.playerRandomMove(); // tu zmieniają się współrzędne dla Gracza

                Position playerNewPosition = player.getPosition();
                BoardField boardNewField = board.findBoardField(playerNewPosition);

                if (player instanceof Cat) {
                    boardOldField.decreaseCatNumber();
                    boardNewField.increaseCatNumber();

                } else {
                    boardOldField.decreaseMouseNumber();
                    boardNewField.increaseMouseNumber();
                }
            }
            board.setNewSgnsOnBoard();
        }
    }

    private void killMice() {
        for (Cat cat : cats) {
            Position catPosition = cat.getPosition();
            for (Mouse mouse : mice) {
                Position mousePosition = mouse.getPosition();
                if (catPosition.equals(mousePosition) && mouse.isAlive()) {
                    mouse.setAlive(false);
                    System.out.printf("Cat %s caught Mouse %s\n", cat.getId(), mouse.getId());
                    board.findBoardField(mousePosition).decreaseMouseNumber();
                }
            }
        }
    }

    private Mouse findMouseOnCheesePosition() {
        for (Mouse mouse : mice) {
            if (mouse.getPosition().equals(Board.cheesePosition)) {
                return mouse;
            }
        }
        return null;
    }

    private int countAliveMice() {
        int count = 0;
        for (Mouse mouse : mice) {
            if (mouse.isAlive()) {
                count++;
            }
        }
        return count;
    }


    private void locateMiceRandomly() {
        for (int i = 0; i < mice.length; i++) {
            int randomX = random.nextInt(Board.BOARD_SIZE);
            int randomY = random.nextInt(Board.BOARD_SIZE);
            Position position = new Position(randomX, randomY);
            if (randomY == 0 || randomX == 0 || randomY == Board.BOARD_SIZE - 1 || randomX == Board.BOARD_SIZE - 1) {
                BoardField positionOnBoard = board.findBoardField(position);
                if (positionOnBoard.getSgn().equals("_")) {
                    Mouse mouse = new Mouse();
                    mice[i] = mouse;
                    mice[i].setPosition(position);
                    positionOnBoard.setSgn("M");
                    positionOnBoard.increaseMouseNumber();
                } else {
                    i--;
                }
            } else {
                i--;
            }
        }
    }

    private void locateMiceEvenly() {
        int miceNumber = mice.length;
        Position[] positions = board.generateEdgePositions();
        int frequency = (int) Math.round(positions.length / (double)miceNumber);
        for (int i = 0; i < miceNumber; i++) {
            Mouse mouse = new Mouse();
            Position micePosition = positions[(i * frequency) % positions.length];
            BoardField boardFieldWithMouse = board.findBoardField(micePosition);
            mouse.setPosition(micePosition);
            boardFieldWithMouse.increaseMouseNumber();
            boardFieldWithMouse.setSgn("M");
            mice[i] = mouse;
        }
    }

    private void locateCats() {
        for (int i = 0; i < cats.length; i++) {
            int randomX = random.nextInt(Board.BOARD_SIZE);
            int randomY = random.nextInt(Board.BOARD_SIZE);
            Position position = new Position(randomX, randomY);
            BoardField positionOnBoard = board.findBoardField(position);
            if (positionOnBoard.getSgn().equals("_") || positionOnBoard.getSgn().equals("K")) {
                Cat cat = new Cat();
                cats[i] = cat;
                cats[i].setPosition(position);
                positionOnBoard.setSgn("K");
                positionOnBoard.increaseCatNumber();
            } else {
                i--;
            }
        }
    }

    private Player[] combinePlayers() {
        int length = cats.length + mice.length;
        Player[] players = new Player[length];
        System.arraycopy(cats, 0, players, 0, cats.length);
        System.arraycopy(mice, 0, players, cats.length, mice.length);
        return players;
    }
}