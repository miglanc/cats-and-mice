package catsandmouse.game;

import catsandmouse.Position;

public class BoardField {

    private Position position;
    private String sgn;
    private int catsNumber;
    private int miceNumber;

    BoardField() {
        this.position = new Position();
        this.catsNumber = 0;
        this.miceNumber = 0;
        this.sgn = "_";
    }

    void increaseCatNumber() {
        this.catsNumber++;
    }

    void decreaseCatNumber() {
        this.catsNumber--;
    }

    void increaseMouseNumber() {
        this.miceNumber++;
    }

    void decreaseMouseNumber() {
        this.miceNumber--;
    }

    @Override
    public String toString() {
        return String.format("%s | %s | cats: %s | mice: %s", position, sgn, catsNumber, miceNumber);
    }

    // getters and setters ==================================

    Position getPosition() {
        return position;
    }

    void setPosition(Position position) {
        this.position = position;
    }

    String getSgn() {
        return sgn;
    }

    void setSgn(String sgn) {
        this.sgn = sgn;
    }

    int getCatsNumber() {
        return catsNumber;
    }

    int getMiceNumber() {
        return miceNumber;
    }
}
