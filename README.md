Cats and mice simulation
---
* There is square board with odd size. `BOARD_SIZE (2 < BOARD_SIZE < 100)`.
* There is cheese in the middle of the board.
* **Evenly** on the edge of the board mice are sitting. `MAX_MICE (1 < MAX_MICE < 100)`.
* Cats are sitting randomly `MAX_CATS (1 < MAX_CATS < 100)`

Simulation is divided into **rounds**. Every cat and mouse moves on the random **adjoining field**.
- Cat has 4 options (up/down/left/right),
- Mouse has 8 options (up/down/left/right/diagonally).
- Cats and mice cannot move outside of the board but they may sit on the cheese field. More than one animal may be placed on one field.
- If mice are on the same field as cat(s), they are caught and removed from the board.

**WINNING**:
- Mice win the game if any of them reach the cheese field.
- Cats win the game if all mice are caught before they reach the cheese field.
- If (after the same round) cat(s) and mouse(mice) reach the cheese field, then we have a tie.

**DESCRIPTION**:
* Mice and cats should be numbered `(Cat A, Cat B, Mouse A, Mouse B, etc.)`
* After each round program should display information about each move like this:\
`Mouse A: (x1,y1) -> (x2,y2)`\
`Cat D: (x1,y1) -> (x2,y2)`
* Additional infos displayed:\
    * In the begining of the round: \
    `Round X - Y mice left`
    * After moves: \
    `Cat B caught Mouse A`\
    `Mouse C reached the cheese`\
    `Cats won!` \
    `Mice won!` \
    `Tie!`




